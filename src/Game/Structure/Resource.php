<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/29/2018
 * Time: 8:38 PM
 */

namespace BST\Game\Structure;


use BST\Game\Contracts\Entity;
use BST\Game\Contracts\Messager;

class Resource extends Entity
{
    protected static $requiredParts = [
        'iron' => [],
        'fire' => [],
        'silicon' => [],
        'copper' => [],
        'carbon' => [],
        'water' => [],
        'fuel' => [],
        'sand' => []
    ];

    public function accept(Messager $messager): string
    {
        return $messager->doForResource($this);
    }
}