<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 1/22/2019
 * Time: 12:12 AM
 */

namespace BST\Game\Structure;
use BST\Game\Contracts\Entity;
use BST\Game\Contracts\Messager;


class CombinedResource extends Entity
{
    protected static $requiredParts = [
        'metal' => ['iron', 'fire']
    ];

    public function accept(Messager $messager): string
    {
        return $messager->doForCombinedResource($this);
    }
}