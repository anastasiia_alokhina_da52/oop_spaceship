<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/29/2018
 * Time: 8:38 PM
 */

namespace BST\Game\Structure;


use BST\Game\Contracts\Entity;
use BST\Game\Contracts\Messager;

class Module extends Entity
{
    protected static $requiredParts = [
        'shell' => [ 'metal', 'fire'],
        'porthole' => ['sand', 'fire'],
        'control_unit' => ['ic', 'wires'],
        'engine' => ['metal', 'carbon', 'fire'],
        'launcher' => ['water', 'fire', 'fuel'],
        'tank' => ['metal', 'fuel'],
        'wires' => ['copper', 'fire'],
        'ic' => ['metal', 'silicon'],
        'spaceship' => ['shell', 'porthole', 'control_unit', 'engine', 'launcher', 'tank']
    ];

    public function accept(Messager $messager): string
    {
        return $messager->doForModule($this);
    }

    public static function addToInventory(Entity $part): void
    {
        $partName = $part->getName();
        if (!self::isInInventory($partName)) {
            parent::addToInventory($part);
        } else {
            throw new \UnexpectedValueException("Attention! " . ucfirst($partName) . ' is ready.');
        }
    }

    public static function isSpaceshipBuilt(): bool
    {
        $spaceship = new Module('spaceship');
        if ($spaceship->getNeededParts() == []) {
            return true;
        } else {
            return false;
        }
    }
}