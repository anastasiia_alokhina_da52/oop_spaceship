<?php

namespace BST\Game\Contracts\Io;

interface Reader
{
    public function read(): string;
    public function getStream();
}
