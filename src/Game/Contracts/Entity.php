<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/29/2018
 * Time: 8:36 PM
 */

namespace BST\Game\Contracts;


use BST\Game\Messagers\NoPartMessager;
use BST\Game\Messagers\NotEnoughPartsMessager;

abstract class Entity
{
    protected static $requiredParts = [];

    protected $name;

    protected static $inventory = [];

    public function __construct(string $name)
    {
        if (static::exists($name)) {
            $this->name = $name;
        } else {
            throw new \UnexpectedValueException($this->accept(new NoPartMessager()));
        }
    }

    protected static function isInInventory(string $part): bool
    {
        return (array_search($part, static::$inventory) === false) ? false : true;
    }

    protected static function exists(string $partName): bool
    {
        return array_key_exists($partName, static::$requiredParts);
    }

    public static function getInventory(): array
    {
        return static::$inventory;
    }

    public static function getRequiredParts(): array
    {
        return static::$requiredParts;
    }

    public static function addToInventory(Entity $part): void
    {
        if ($part->constructPart()) {
            static::$inventory[] = $part->getName();
        } else {
            throw new \UnexpectedValueException($part->accept(new NotEnoughPartsMessager()));
        }
    }

    public function getName(): string
    {
        return $this->name;
    }
    
    protected function constructPart(): bool
    {
        if ($this->hasAllParts()) {
            foreach (static::$requiredParts[$this->name] as $part) {
                if (($key = array_search($part, static::$inventory)) !== false) {
                    unset(static::$inventory[$key]);
                } else {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    protected function hasAllParts(): bool
    {
        foreach (static::$requiredParts[$this->name] as $part) {
            if (!static::isInInventory($part)) {
                return false;
            }
        }
        return true;
    }

    public function getNeededParts(): array
    {
        $neededParts = [];
        foreach (static::$requiredParts[$this->name] as $part) {
            if (!static::isInInventory($part)) {
                $neededParts[] = $part;
            }
        }
        return $neededParts;
    }

    public function getPartRequiredParts(): array
    {
        return static::$requiredParts[$this->name];
    }

    abstract public function accept(Messager $visitor);
}