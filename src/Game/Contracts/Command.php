<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/28/2018
 * Time: 9:49 PM
 */

namespace BST\Game\Contracts;


interface Command
{
    public function execute(): void;
}