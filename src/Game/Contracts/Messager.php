<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/29/2018
 * Time: 11:17 PM
 */

namespace BST\Game\Contracts;


use BST\Game\Structure\CombinedResource;
use BST\Game\Structure\Module;
use BST\Game\Structure\Resource;

interface Messager
{
    public function doForModule(Module $module): string;

    public function doForResource(Resource $resource): string;

    public function doForCombinedResource(CombinedResource $resource): string;
}