<?php

namespace BST\Game;

use BST\Game\Commands\BuildCommand;
use BST\Game\Commands\HelpCommand;
use BST\Game\Commands\MineCommand;
use BST\Game\Commands\ProduceCommand;
use BST\Game\Commands\SchemeCommand;
use BST\Game\Commands\StatusCommand;
use BST\Game\Contracts\Command;
use BST\Game\Contracts\Io\Reader;
use BST\Game\Contracts\Io\Writer;

class Game
{
    private function createCommand(string $command, string $parameter, Writer $writer): Command
    {
        switch ($command) {
            case 'help':
                return new HelpCommand($writer);
                break;
            case 'status':
                return new StatusCommand($writer);
                break;
            case 'build':
                return new BuildCommand($parameter, $writer);
                break;
            case 'scheme':
                return new SchemeCommand($parameter, $writer);
                break;
            case 'mine':
                return new MineCommand($parameter, $writer);
                break;
            case 'produce':
                return new ProduceCommand($parameter, $writer);
                break;
            case 'exit':
                exit;
            default:
                throw new \UnexpectedValueException("There is no command $command" );
        }
    }

    public function start(Reader $reader, Writer $writer): void
    {
        while (true) {
            $this->run($reader, $writer);
        }
    }

    public function run(Reader $reader, Writer $writer): void
    {
        try {
            $input = explode(':', trim($reader->read()));
            $command = $input[0];
            $parameter = isset($input[1]) ? $input[1] : '';
            $command = $this->createCommand($command, $parameter, $writer);
            $command->execute();
        } catch (\Exception $e) {
            $writer->writeln($e->getMessage());
        }

    }
}
