<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/30/2018
 * Time: 12:28 AM
 */

namespace BST\Game\Messagers;


use BST\Game\Contracts\Messager;
use BST\Game\Structure\CombinedResource;
use BST\Game\Structure\Module;
use BST\Game\Structure\Resource;

class AddedMessager implements Messager
{
    public function doForModule(Module $module): string
    {
        $moduleName = ucfirst($module->getName());
        return "$moduleName is ready!";
    }

    public function doForResource(Resource $resource): string
    {
        $resourceName = ucfirst($resource->getName());
        return "$resourceName added to inventory.";
    }

    public function doForCombinedResource(CombinedResource $resource): string
    {
        $resourceName = ucfirst($resource->getName());
        return "$resourceName added to inventory.";
    }
}