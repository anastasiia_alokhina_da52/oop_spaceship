<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/30/2018
 * Time: 12:28 AM
 */

namespace BST\Game\Messagers;


use BST\Game\Contracts\Messager;
use BST\Game\Structure\CombinedResource;
use BST\Game\Structure\Module;
use BST\Game\Structure\Resource;

class NotEnoughPartsMessager implements Messager
{
    public function doForModule(Module $module): string
    {
        $message = "Inventory should have: ";
        foreach ($module->getNeededParts() as $neededPart) {
            $message .= $neededPart . ",";
        }
        return $this->replaceLastComma($message);
    }

    public function doForResource(Resource $resource): string
    {
        return "";
    }

    public function doForCombinedResource(CombinedResource $resource): string
    {
        $message = "You need to mine: ";
        foreach ($resource->getNeededParts() as $neededPart) {
            $message .= $neededPart . ",";
        }
        return $this->replaceLastComma($message);
    }

    private function replaceLastComma(string $message): string
    {
        if ($message{strlen($message)-1} == ',') {
            $message{strlen($message)-1} = '.';
        }
        return $message;
    }
}