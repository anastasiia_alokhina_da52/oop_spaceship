<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/29/2018
 * Time: 11:20 PM
 */

namespace BST\Game\Messagers;


use BST\Game\Contracts\Messager;
use BST\Game\Structure\CombinedResource;
use BST\Game\Structure\Module;
use BST\Game\Structure\Resource;

class NoPartMessager implements Messager
{
    public function doForModule(Module $module): string
    {
        return 'There is no such spaceship module.';
    }

    public function doForResource(Resource $resource): string
    {
        return 'No such resource.';
    }

    public function doForCombinedResource(CombinedResource $resource): string
    {
        return 'No such resource.';
    }
}