<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/28/2018
 * Time: 10:14 PM
 */

namespace BST\Game\Commands;


use BST\Game\Contracts\Command;
use BST\Game\Contracts\Io\Writer;
use BST\Game\Messagers\AddedMessager;
use BST\Game\Structure\Module;

class BuildCommand implements Command
{
    private $moduleName;
    private $writer;

    public function __construct(string $moduleName, Writer $writer)
    {
        $this->moduleName = $moduleName;
        $this->writer = $writer;
    }

    public function execute(): void
    {
        $module = new Module($this->moduleName);
        Module::addToInventory($module);
        $message = $module->accept(new AddedMessager());
        if (Module::isSpaceshipBuilt()) {
            $message .= ' => You won!';
        }
        $this->writer->writeln($message);
    }
}