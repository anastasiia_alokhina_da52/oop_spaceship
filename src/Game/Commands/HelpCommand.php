<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/28/2018
 * Time: 9:59 PM
 */

namespace BST\Game\Commands;


use BST\Game\Contracts\Command;
use BST\Game\Contracts\Io\Writer;

class HelpCommand implements Command
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function execute(): void
    {
        $this->writer->writeln('Available commands: help, status, build, scheme, mine, produce, exit.');
    }
}