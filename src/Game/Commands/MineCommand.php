<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/28/2018
 * Time: 10:14 PM
 */

namespace BST\Game\Commands;


use BST\Game\Contracts\Command;
use BST\Game\Contracts\Io\Writer;
use BST\Game\Messagers\AddedMessager;
use BST\Game\Structure\Resource;

class MineCommand implements Command
{
    private $resourceName;
    private $writer;

    public function __construct(string $resourceName, Writer $writer)
    {
        $this->resourceName = $resourceName;
        $this->writer = $writer;
    }

    public function execute(): void
    {
        $resource = new Resource($this->resourceName);
        Resource::addToInventory($resource);
        $this->writer->writeln($resource->accept(new AddedMessager()));
    }
}