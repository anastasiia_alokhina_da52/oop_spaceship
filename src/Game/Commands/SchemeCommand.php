<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/28/2018
 * Time: 10:15 PM
 */

namespace BST\Game\Commands;


use BST\Game\Contracts\Command;
use BST\Game\Contracts\Io\Writer;
use BST\Game\Structure\Module;

class SchemeCommand implements Command
{
    private $moduleName;
    private $writer;

    public function __construct(string $moduleName, Writer $writer)
    {
        $this->moduleName = $moduleName;
        $this->writer = $writer;
    }

    public function execute(): void
    {
        $module = new Module($this->moduleName);
        $requiredParts = $module->getPartRequiredParts();
        $this->writer->writeln(ucfirst($this->moduleName) . ' => ' . implode($requiredParts, '|'));
    }
}