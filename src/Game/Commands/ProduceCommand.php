<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/30/2018
 * Time: 12:38 AM
 */

namespace BST\Game\Commands;


use BST\Game\Contracts\Command;
use BST\Game\Contracts\Io\Writer;
use BST\Game\Messagers\AddedMessager;
use BST\Game\Structure\CombinedResource;

class ProduceCommand implements Command
{
    private $resourceName;
    private $writer;

    public function __construct(string $resourceName, Writer $writer)
    {
        $this->resourceName = $resourceName;
        $this->writer = $writer;
    }

    public function execute(): void
    {
        $resource = new CombinedResource($this->resourceName);
        CombinedResource::addToInventory($resource);
        $this->writer->writeln($resource->accept(new AddedMessager()));
    }
}