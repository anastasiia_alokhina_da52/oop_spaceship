<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/28/2018
 * Time: 10:05 PM
 */

namespace BST\Game\Commands;


use BST\Game\Contracts\Command;
use BST\Game\Contracts\Entity;
use BST\Game\Contracts\Io\Writer;

class StatusCommand implements Command
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function execute(): void
    {
        $this->writer->writeln('You have:');
        foreach (Entity::getInventory() as $part)
        {
            $this->writer->writeln( $part);
        }
    }
}