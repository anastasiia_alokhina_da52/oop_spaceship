<?php

require __DIR__ . '/vendor/autoload.php';

$reader = new \BST\Game\Io\CliReader();
$writer = new \BST\Game\Io\CliWriter();

$game = new \BST\Game\Game;

$game->start($reader, $writer);
